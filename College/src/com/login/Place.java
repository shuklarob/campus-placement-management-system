package com.login;

import java.io.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Place
 */
@WebServlet("/Place")
public class Place extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String tenth = (request.getParameter("tenth"));
		String twelve= (request.getParameter("twelve"));
		String BE= (request.getParameter("BE"));
		String Branch = request.getParameter("Branch");
		String backlog = request.getParameter("backlog");
		String m,z=null;
		String n;
		String temp = "YES";
		String temp1 ="Information Science";
		int i = 1;
		
		if(backlog.equals(temp))
		{
			if(Branch.equals("ALL"))
			{
				m = "select student.first_name,student.last_name,student.branch,student.email,qualification.class10mark,qualification.class12marks,semester.sgpa from student join semester on student.usn=semester.usn join qualification on student.usn=qualification.usn where semester.sgpa>BE and qualification.class10mark>tenth and qualification.class12marks>twelve and semester.backlog='baack' order by student.branch";
				m = m.replace("BE",BE);
				m = m.replace("tenth",tenth);
				m = m.replace("twelve",twelve);
				m = m.replace("baack",backlog);
	//			m = m.replace("brranch",Branch);
				z =m;
			}
			
			else
			{
				m = "select student.first_name,student.last_name,student.branch,student.email,qualification.class10mark,qualification.class12marks,semester.sgpa from student join semester on student.usn=semester.usn join qualification on student.usn=qualification.usn where semester.sgpa>BE and qualification.class10mark>tenth and qualification.class12marks>twelve and student.branch='brranch'and semester.backlog='baack' order by student.branch";
				m = m.replace("BE",BE);
				m = m.replace("tenth",tenth);
				m = m.replace("twelve",twelve);
				m = m.replace("baack",backlog);
				m = m.replace("brranch",Branch);
				z =m;
			}
		}
		else
		{
			if(Branch.equals("ALL"))
			{
				m = "select student.first_name,student.last_name,student.branch,student.email,qualification.class10mark,qualification.class12marks,semester.sgpa from student join semester on student.usn=semester.usn join qualification on student.usn=qualification.usn where semester.sgpa>BE and qualification.class10mark>tenth and qualification.class12marks>twelve and semester.backlog='baack' order by student.branch";
				m = m.replace("BE",BE);
				m = m.replace("tenth",tenth);
				m = m.replace("twelve",twelve);
				m = m.replace("baack",backlog);
	//			m = m.replace("brranch",Branch);
				z =m;
			}
			else
			{
				m = "select student.first_name,student.last_name,student.branch,student.email,qualification.class10mark,qualification.class12marks,semester.sgpa from student join semester on student.usn=semester.usn join qualification on student.usn=qualification.usn where semester.sgpa>BE and qualification.class10mark>tenth and qualification.class12marks>twelve and student.branch='brranch'and semester.backlog='baack' order by student.branch";
				m = m.replace("BE",BE);
				m = m.replace("tenth",tenth);
				m = m.replace("twelve",twelve);
				m = m.replace("baack",backlog);
				m = m.replace("brranch",Branch);
				z =m;
			}
		}
		PrintWriter out =response.getWriter();
		String sql = "select * from student";
		String DB_URL = "jdbc:mysql://localhost:3306/college";
		String USER = "root";
		String PASS = "qwerty";
			try{
			    Class.forName("com.mysql.jdbc.Driver");
				Connection con= DriverManager.getConnection(DB_URL,USER,PASS);
				PreparedStatement st = con.prepareStatement(z);
	
			 
				
				ResultSet rs = st.executeQuery();
				
				String str = "<h2><b>Selected Students</b></h2><br><table border=1><tr><th>First_name</td><th>Last_name</th><th>Branch</th><th>Email</th><th>10thmarks</th><th>12thmarks</th><th>SGPA</th></tr>";
				while(rs.next())
				{
				str += "<tr><td>"+rs.getString(1)+"</td><td>"+rs.getString(2)+"</td><td>"+rs.getString(3)+"</td><td>"+rs.getString(4)+"</td><td>"+rs.getString(5)+"</td><td>"+rs.getString(6)+"</td><td>"+rs.getString(7)+"</td>";
				}
				str += "</table>";
				out.println(str);
				con.close();
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	}
}
