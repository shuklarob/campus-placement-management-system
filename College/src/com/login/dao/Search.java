package com.login.dao;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Search
 */
@WebServlet("/Search")
public class Search extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		
		String usn = (request.getParameter("search"));
		
		String m,sql;
		
		m="select student.usn,student.first_name,student.last_name,student.branch,student.email,qualification.class10mark,qualification.class12marks,semester.sgpa from student join semester on student.usn=semester.usn join qualification on student.usn=qualification.usn where student.usn='xyz'";
		m=m.replace("xyz",usn);
		sql=m;
		
		PrintWriter out =response.getWriter();
		String DB_URL = "jdbc:mysql://localhost:3306/college";
		String USER = "root";
		String PASS = "qwerty";
			try{
			    Class.forName("com.mysql.jdbc.Driver");
				Connection con= DriverManager.getConnection(DB_URL,USER,PASS);
				PreparedStatement st = con.prepareStatement(sql);
	
				ResultSet rs = st.executeQuery();
				
				String str = "<h2><b>Student details</b></h2><br><table border=1><tr><th>USN</td><th>First_name</td><th>Last_name</th><th>Branch</th><th>Email</th><th>10thmarks</th><th>12thmarks</th><th>SGPA</th></tr>";
				while(rs.next())
				{
				str += "<tr><td>"+rs.getString(1)+"</td><td>"+rs.getString(2)+"</td><td>"+rs.getString(3)+"</td><td>"+rs.getString(4)+"</td><td>"+rs.getString(5)+"</td><td>"+rs.getString(6)+"</td><td>"+rs.getString(7)+"</td><td>"+rs.getString(8)+"</td>";
				}
				str += "</table>";
				out.println(str);
				con.close();
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	}

	
	}

