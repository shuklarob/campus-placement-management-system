package com.login;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class Details
 */
@WebServlet("/Details")
public class Details extends HttpServlet {
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	
		String Branch = (request.getParameter("Branch"));
		String m,z;
		
		if(Branch.equals("ALL"))
		{
		m = "select student.usn,student.branch,student.first_name,student.last_name,student.birth,student.email,student.mobile,student.gender,qualification.class10,qualification.class10mark,qualification.pass10year,qualification.class12,qualification.class12marks,qualification.pass12year,semester.first,semester.second,semester.third,semester.fourth,semester.fifth,semester.sixth,semester.backlog,semester.sgpa from student join semester on student.usn=semester.usn join qualification on student.usn=qualification.usn order by student.branch";
		z=m;
		}
		else
		{
			m = "select student.usn,student.branch,student.first_name,student.last_name,student.birth,student.email,student.mobile,student.gender,qualification.class10,qualification.class10mark,qualification.pass10year,qualification.class12,qualification.class12marks,qualification.pass12year,semester.first,semester.second,semester.third,semester.fourth,semester.fifth,semester.sixth,semester.backlog,semester.sgpa from student join semester on student.usn=semester.usn join qualification on student.usn=qualification.usn where student.branch='brranch'";
			m = m.replace("brranch",Branch);
			z =m;
		}
		
		
		
		
		PrintWriter out =response.getWriter();
		String sql = "select * from student";
		String DB_URL = "jdbc:mysql://localhost:3306/college";
		String USER = "root";
		String PASS = "qwerty";
			try{
			    Class.forName("com.mysql.jdbc.Driver");
				Connection con= DriverManager.getConnection(DB_URL,USER,PASS);
				PreparedStatement st = con.prepareStatement(z);
	
			 
				
				ResultSet rs = st.executeQuery();
				
				String str = "<h2><b>Students Details</b></h2><br><table border=1><tr><th>USN</td><th>Branch</th><th>First_name</th><th>Last_name</th><th>Date_Of_Birth</th><th>Email</th><th>Mobile</th><th>Gender</th><th>10thBoard</th><th>10thmarks</th><th>Year_of_passing</th><th>12thBoard</th><th>12thmarks</th><th>Year_of_Passing</th><th>SEM1</th><th>SEM2</th><th>SEM3</th><th>SEM4</th><th>SEM5</th><th>SEM6</th><th>BACKLOG</th><th>SGPA</th></tr>";
				while(rs.next())
				{
				str += "<tr><td>"+rs.getString(1)+"</td><td>"+rs.getString(2)+"</td><td>"+rs.getString(3)+"</td><td>"+rs.getString(4)+"</td><td>"+rs.getString(5)+"</td><td>"+rs.getString(6)+"</td><td>"+rs.getString(7)+"</td><td>"+rs.getString(8)+"</td><td>"+rs.getString(9)+"</td><td>"+rs.getString(10)+"</td><td>"+rs.getString(11)+"</td><td>"+rs.getString(12)+"</td><td>"+rs.getString(13)+"</td><td>"+rs.getString(14)+"</td><td>"+rs.getString(15)+"</td><td>"+rs.getString(16)+"</td><td>"+rs.getString(17)+"</td><td>"+rs.getString(18)+"</td><td>"+rs.getString(19)+"</td><td>"+rs.getString(20)+"</td><td>"+rs.getString(21)+"</td><td>"+rs.getString(22)+"</td>";
				}
				str += "</table>";
				out.println(str);
				con.close();
			} catch (Exception e) {
				
				e.printStackTrace();
			}
	}

}

