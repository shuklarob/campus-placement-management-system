package com.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.login.dao.CompanyDao;
import com.login.dao.RegisterDao;
import com.login.dao.SignDao;
import com.login.dao.SignRegDao;

/**
 * Servlet implementation class Signup
 */
@WebServlet("/Signup")
public class Signup extends HttpServlet {
protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String fname = request.getParameter("first_name");
		String lname = request.getParameter("last_name");
		String userid = request.getParameter("userid");
		String pass = request.getParameter("password");
		
		SignDao dao7 = new SignDao();
		
		
		if(dao7.check(userid))
		{
			response.sendRedirect("company_id.jsp");
		}
		else
		{
			SignRegDao dao8 = new SignRegDao();
		dao8.insert(fname,lname,userid,pass);
		response.sendRedirect("placement.jsp");
	}
	}
}