package com.studentreg;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.login.dao.LoginDao;
import com.login.dao.QualiDao;
import com.login.dao.SemDao;
import com.login.dao.SgpaDao;
import com.login.dao.StudentDao;
import com.login.dao.UsnDao;


@WebServlet("/StudentReg")
public class StudentReg extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		String First_Name = request.getParameter("First_Name");
	//	String Last_Name = request.getParameter("Last_Name");
	//	String USN = request.getParameter("USN");
		String Birthday_day = request.getParameter("Birthday_day");
		String Birthday_Month = request.getParameter("Birthday_Month");
		String Birthday_Year = request.getParameter("Birthday_Year");
	//	String Email_Id = request.getParameter("Email_Id");
	//	String Mobile_Number = request.getParameter("Moblie_Number");
	//	String Gender = request.getParameter("Gender");
	//	String ClassX_Board= request.getParameter("ClassX_Board");
	//	String ClassX_Percentage= request.getParameter("ClassX_Percentage");
	//	String ClassX_YrOfPassing= request.getParameter("ClassX_YrOfPassing");
	//	String ClassXII_Board= request.getParameter("ClassXII_Board");
	//	String ClassXII_Percentage= request.getParameter("ClassXII_Percentage");
	//	String ClassXII_YrOfPassing= request.getParameter("ClassXII_YrOfPassing");
	//	String first = request.getParameter("1st");
	//	String second = request.getParameter("2nd");
	//	String third = request.getParameter("3rd");
	//	String fourth = request.getParameter("4th");
	//	String backlog =(request.getParameter("backlog"));
		
		String i =(request.getParameter("First_Name"));
		String j =(request.getParameter("Last_Name"));
		String k =(request.getParameter("USN"));
		String b =(request.getParameter("branch"));
		String l =(request.getParameter("email"));
		String m =(request.getParameter("Mobile_Number"));
		String n =(request.getParameter("Gender"));
		String o =(request.getParameter("ClassX_Board"));
		float p =Float.parseFloat(request.getParameter("ClassX_Percentage"));
		int q =Integer.parseInt(request.getParameter("ClassX_YrOfPassing"));
		String r =(request.getParameter("ClassXII_Board"));
		float s =Float.parseFloat(request.getParameter("ClassXII_Percentage"));
		int t =Integer.parseInt(request.getParameter("ClassXII_YrOfPassing"));
		float u =Float.parseFloat(request.getParameter("1st"));
		float v =Float.parseFloat(request.getParameter("2nd"));
		float w =Float.parseFloat(request.getParameter("3rd"));
		float x =Float.parseFloat(request.getParameter("4th"));
		float c =Float.parseFloat(request.getParameter("5th"));
		float d =Float.parseFloat(request.getParameter("6th"));
		String f =(request.getParameter("backlog"));
	
		
		String y = Birthday_day+Birthday_Month+Birthday_Year;
		
		UsnDao dao5 = new UsnDao();
		
		if(dao5.check(k))
		{
			response.sendRedirect("usn.jsp");
		}
		
		else
		{
			StudentDao dao2 = new StudentDao();
			dao2.insert2(i,j,k,b,y,l,m,n);
			
			QualiDao dao3 = new QualiDao();
			dao3.insert3(k,o,p,q,r,s,t);
			
			SemDao dao4 = new SemDao();
			dao4.insert4(k,u,v,w,x,c,d,f);
			
			SgpaDao dao6 = new SgpaDao();
			dao6.sgpa();
			
			response.sendRedirect("cong.jsp");
		}	
		
	}

	
}
