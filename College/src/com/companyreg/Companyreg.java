package com.companyreg;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.login.dao.CompanyDao;
import com.login.dao.LoginDao;
import com.login.dao.RegisterDao;
import com.login.dao.UsnDao;

@WebServlet("/Companyreg")
public class Companyreg extends HttpServlet {
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String name = request.getParameter("name");
		String company_id = request.getParameter("Company_id");
		String email = request.getParameter("email");
		String student = request.getParameter("nostudent");
		
		CompanyDao dao6 = new CompanyDao();
		
		if(dao6.check(company_id))
		{
			response.sendRedirect("company_id.jsp");
		}
		else
		{
		RegisterDao dao1 = new RegisterDao();
		dao1.insert(name, company_id,email,student);
		response.sendRedirect("placement.jsp");
	}
	}
}
