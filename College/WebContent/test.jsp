<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<title>Login Form Design</title>
    <link rel="stylesheet" type="text/css" href="logcompany.css">
<body>
    <div class="loginbox">
    <img src="avatar.png" class="avatar">
        <h1>Company Login</h1>
        <form action="test">
            <p>Username</p>
            <input type="text" name="uname" placeholder="Enter Username">
            <p>Password</p>
            <input type="password" name="pass" placeholder="Enter Password">
            <input type="submit" name="" value="Login">
            <a href="#">Lost your password?</a><br>
            <a href="regcompany.jsp">Don't have an account?</a>
        </form>
        
    </div>

</body>
</head>
</html>