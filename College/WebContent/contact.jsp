<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <title></title>
</head>
<body>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<h1 style="text-align:center;">
  <img src="logo.jpg" alt="Trulli" width="150" height="150">
  <b>UNIVERSITY OF INDIA</b></h1>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="College.jsp"><b>Home</b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <b>About Us</b>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="manage.jsp">Management</a>
          <a class="dropdown-item" href="prin.jsp">Principal</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="vision.jsp">Vision and Mission</a>
        </div>
      </li>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="login.jsp"><b>Placements</b> <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="stureg.jsp"><b>Students</b></a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="contact.jsp"><b>Contact Us</b></a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
	<h2 align="center">Placements</h2><br>
	<h3>Department of Training & Placement</h3><br>
	<p>Students of University of India have excellent academic records and are placed in esteemed industries and corporate sectors. The graduates and post graduate students of University of India are continuously trained on aptitude, technical and soft skills. Seminars, workshops and faculty development program are conducted on a regular basis to update the advances in the field of engineering. Student’s forum like Rotract Club, TEDx, XKalibre and quiz club conduct various technical, cultural and sports events for faculty and students. These activities create an awareness to understand and solve professional and social problems.</p><br>

	<h3 align="center">Contact</h3><br>
	<h3 align="center">Ph: 080-22422741</h3><br>
	<h3 align="center">Email: uoiplacement@yahoo.com</h3>
</body>
</html>
