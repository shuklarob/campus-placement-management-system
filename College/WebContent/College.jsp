<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
  <title></title>
</head>
<body>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<h1 style="text-align:center;">
  <img src="logo.jpg" alt="Trulli" width="150" height="150">
  <b>UNIVERSITY OF INDIA</b></h1>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#"><b>Home</b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <b>About Us</b>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="manage.jsp">Management</a>
          <a class="dropdown-item" href="prin.jsp">Principal</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="vision.jsp">Vision and Mission</a>
        </div>
      </li>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="login.jsp"><b>Placements</b> <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="stureg.jsp"><b>Students</b></a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="contact.jsp"><b>Contact Us</b></a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<img src="architecture-buildings-campus-220351.jpg" alt="Trulli" width="1287" height="500">
<p><i>UNIVERSITY OF INDIA</i> was started in the year 1980 with the Intention of providing quality education in the field of technology and thereby serving our society. It has reached enviable level of excellence in technical education. This was achieved by our staff and committed students, as well as very good infrastructure facilities provided by the management. The atmosphere in the college is quite conductive for learning and practicing the various disciplines of the Engineering. The all-round development of a student is given top priority in college.
  <br>
<br>
<i>UNIVERSITY</i> OF INDIA has always been at the forefront of modern technology and is credited with the introduction of the first full-fledged COMPUTER SCIENCE & ENGINEERING degree in INDIA. Thus, the institute has identified future growth areas and incorporated various courses over the years. At present UNIVERSITY OF INDIA has 9 undergraduate, 10 post graduate and Ph.D. course(s) and the intake of UNIVERSITY OF INDIA is more than 4000.
<br>
<br>
The institute offers the Bachelor of Engineering (B.E.) Degree in Civil, Mechanical, Electrical and Electronics, Electronics and Communication, Computer Science, Electronics and Instrumentation Technology, Telecommunication, Information Science and Engineering and Industrial Engineering and Management.
<br>
<br>
There are a number of centers carrying out inter-disciplinary research and many collaborative programs exist between the college and other institutions, like IISc, N.A.L, etc. The chemistry department has been recognized as one of the latest research centers in the state of Karnataka. The college is also a program center for IGNOU. Teaching & research work is also amply supported by facilities such as central library with 90,000 volumes of text books and reference books, and an INDUSTRIAL CONSULTANCY AND RESEARCH CENTER. Besides this, the institute also offers the facilities like auditorium (1200 seating capacity), seminar hall, co-operative society, canteen, full-fledged sports and cultural sections as well as hostels for boys and girls. 
</p>
<div class="card-deck">
  <div class="card">
    <img class="card-img-top" src="accomplishment-celebrate-ceremony-267885.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">News and Events</h5>
      <p class="card-text">To nurture the cultural and technical talents of UNIVERSITY OF INDIA students, college conducts annual fest called MANTHAN and also to highlight the glorious years of college history we bring out periodical magazine UIANNICA.</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="archive-beautiful-book-stack-256455.jpg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Library</h5>
      <p class="card-text">Library serves as a resource centre and aims to develop a comprehensive collection of document’s useful for faculty and students of the institute and provides an efficient dissemination of knowledge.</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>
  <div class="card">
    <img class="card-img-top" src="pexels-photo-288477.jpeg" alt="Card image cap">
    <div class="card-body">
      <h5 class="card-title">Placement</h5>
      <p class="card-text">Students of UNIVERSITY OF INDIA have excellent academic records and are placed in esteemed industries and corporate sectors. The graduates and post graduate students of UNIVERSITY OF INDIA are continuously trained on aptitude, technical and soft skills...</p>
      <p class="card-text"><small class="text-muted">Last updated 3 mins ago</small></p>
    </div>
  </div>
</div>

</body>
</html>