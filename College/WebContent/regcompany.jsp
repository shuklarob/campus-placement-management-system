<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
* {box-sizing: border-box;}

body {
  margin: 0;
  font-family: Arial, Helvetica, sans-serif;
}

.topnav {
  overflow: hidden;
  background-color: #e9e9e9;
}

.topnav a {
  float: left;
  display: block;
  color: black;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
  font-size: 17px;
}

.topnav a:hover {
  background-color: #ddd;
  color: black;
}

.topnav a.active {
  background-color: #2196F3;
  color: white;
}

.topnav .search-container {
  float: right;
}

.topnav input[type=text] {
  padding: 6px;
  margin-top: 8px;
  font-size: 17px;
  border: none;
}

.topnav .search-container button {
  float: right;
  padding: 6px 10px;
  margin-top: 8px;
  margin-right: 16px;
  background: #ddd;
  font-size: 17px;
  border: none;
  cursor: pointer;
}

.topnav .search-container button:hover {
  background: #ccc;
}

@media screen and (max-width: 600px) {
  .topnav .search-container {
    float: none;
  }
  .topnav a, .topnav input[type=text], .topnav .search-container button {
    float: none;
    display: block;
    text-align: left;
    width: 100%;
    margin: 0;
    padding: 14px;
  }
  .topnav input[type=text] {
    border: 1px solid #ccc;  
  }
}
</style>
</head>
<body>

<div class="topnav">
  <a class="active" href="College.jsp">HOME</a>
  <a href="delete.jsp">Update</a>
  <a href="details.jsp">Student</a>
  <a href="regcompany.html">Company</a>
  <a href="comp.jsp">Company Details</a>
  <a href="signup.jsp">SignUp</a>
  
  <div class="search-container">
    <form action="Search">
      <input type="text" placeholder="Search.." name="search">
      <button type="submit"><i class="fa fa-search"></i></button>
    </form>
  </div>
</div>


<h2 align="center"><u>COMPANY</u></h2>
<form action="Companyreg" method="post">
<b>Company Name</b><br>
<input type="text" name="name" required>
<br>
<br>
<b>Company ID</b><br>
<input type="text" name="Company_id" required>
<br>
<br>
<b>Company email</b><br>
<input type="text" name="email" required>
<br>
<br>
<tr>
<b>No. of student selected</b><br>
<input type="text" name="nostudent" required><br>
<br><br>
<input type="submit" name="" value="submit">
</form>
</body>
</html>
