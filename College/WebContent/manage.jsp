<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
	<link rel="stylesheet" type="text/css" href="principal.css">
	<title></title>
	 <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
<h1 style="text-align:center;">
  <img src="logo.jpg" alt="Trulli" width="150" height="150">
  <b>UNIVERSITY OF INDIA</b></h1>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="College.jsp"><b>Home</b></a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
<li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
          <b>About Us</b>
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="manage.jsp">Management</a>
          <a class="dropdown-item" href="prim.jsp">Principal</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="vision.jsp">Vision and Mission</a>
        </div>
      </li>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="login.jsp"><b>Placements</b> <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="stureg.jsp"><b>Students</b></a>
      </li>
      <li class="nav-item">
        <a class="nav-link disabled" href="contact.jsp"><b>Contact Us</b></a>
      </li>
    </ul>
    <form class="form-inline my-2 my-lg-0">
      <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
      <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
    </form>
  </div>
</nav>
<br>
<br>
<h2>Management</h2><br>
<h3>About Management</h3><br>
<p>Our management, RAJYA Vokkaligara Sangha was founded in the year 1906 to promote social, cultural and educational aspirations of the agricultural community. Rajya Vokkaligara Sangha with a commanding presence in Bangalore, Mandya, Mysore, Hassan, Tumkur, and Kolar districts and with a fairly well spread presence in other regions of Karnataka has been conducting number of regular activities for the development and well being of the community in particular and to all sections of the society in general. The sangha has also provide hostel facilities for poor background student and students are making use of this hostel facilities during their education carrier. The Rajya Vokkaligara Sangha comprise of 35 members, headed by
</p>
</body>
</html>
 
